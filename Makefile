CC=gcc
CFLAGS=-Wall -Wextra -Wpedantic -Waggregate-return -Wwrite-strings -Wvla -Wfloat-equal -std=c17

SDIR=src
ODIR=obj
BDIR=bin
IDIR=include

LIBS=-L/usr/lib -lssl -lcrypto -pthread

CFLAGS+=-I$(IDIR)

_SERVER_OBJS=echo_server.o
SERVER_OBJS=$(patsubst %,$(ODIR)/%,$(_SERVER_OBJS))

echo_server: $(SERVER_OBJS)
	echo "Generating Certificates"
	bash generate_certs.sh > /dev/null 2>/dev/null
	mkdir -p $(BDIR) $(ODIR)
	$(CC) -o $(BDIR)/$@ $^ $(CFLAGS) $(LIBS)

$(ODIR)/%.o: $(SDIR)/%.c $(DEPS)
	mkdir -p $(BDIR) $(ODIR)
	$(CC) -c -o $@ $< $(CFLAGS)

.PHONY: clean
clean:
	-rm -rf bin obj vgcore.* certs

.PHONY: debug
debug: CFLAGS+=-g
debug: echo_server
