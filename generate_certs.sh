#!/usr/bin/bash

rm -r certs
mkdir certs

# Generate all keys.
# TODO: make this skip if already generated, as this takes the longest time
openssl genrsa -out certs/ca_key.pem 2048
openssl genrsa -out certs/server_key.pem 2048
openssl genrsa -out certs/client_key.pem 2048

# Generate signing certificate authority certificate
openssl req -x509 -new -nodes -key certs/ca_key.pem -days 10 -out certs/ca_cert.pem -subj /C=US/ST=ZZ/L=Somewhere/O=aperson/CN=ca_cn

# Generate server certificate
# Canonical Name must be different from certificate authority
openssl req -new -nodes -key certs/server_key.pem -out certs/server.csr -days 10 -subj /C=US/ST=ZZ/L=Somewhere/O=aperson/CN=serv_cn
openssl x509 -req -in certs/server.csr -CA certs/ca_cert.pem -CAkey certs/ca_key.pem -CAcreateserial -out certs/server_cert.pem -days 10


# Generate client certificate
# Canonical Name must be different from certificate authority
openssl req -new -nodes -key certs/client_key.pem -out certs/client.csr -days 10 -subj /C=US/ST=ZZ/L=Somewhere/O=aperson/CN=client_cn
openssl x509 -req -in certs/client.csr -CA certs/ca_cert.pem -CAkey certs/ca_key.pem -CAcreateserial -out certs/client_cert.pem -days 10
