#Echo SSL
This project demonstrates the openssl library in C and ssl in python through an echo server.

##Running Server
run make
execute binary at bin/echo\_server

##Running Python Client
execute echo\_client.py
