#!/usr/bin/python
"""Basic Python SSL echo client."""
import socket
import ssl


def get_user_input():
    """Return user input string from stdin."""
    user_input = None
    while ((user_input is None) or (len(user_input) == 0)):
        try:
            user_input = input()
        except EOFError:
            print("End of file")
            return None
    return user_input


def echo_loop(client_ssl_s):
    """Run the echo loop with a given pre-established ssl connection."""
    # Allow for initial message to be sent from the server
    len_sent = 1
    keep_sending = True
    try:
        while (keep_sending):
            buffer = b""
            len_recv = 0
            # TODO: Open socket in non-blocking state and read until there
            # is nothing in the buffer. Currently, if a WALL message is
            # sent, then the echo received back will fall behind by 1 message.
            buffer = client_ssl_s.recv(1024)
            len_recv += len(buffer)
            if (len_recv == 0):
                break
            print(buffer.decode())
            # Print decoded message from socket
            message = get_user_input()
            # Quit if no user input is returned
            if message is None:
                break
            # Quit on input 'quit'
            if message == "quit":
                keep_sending = False
            enc_message = message.encode()
            len_sent = client_ssl_s.send(enc_message)
    except UnicodeDecodeError as e:
        print(e)


def main():
    """SSL Echo client."""
    # TODO: Take command line arguments for connecting socket
    server_socket = 5555
    server_ip = "127.0.0.1"
    client_cert = "certs/client_cert.pem"
    client_key = "certs/client_key.pem"
    ca_file = "certs/ca_cert.pem"

    try:
        ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_CLIENT)
        # Load signing certificate authority
        ssl_context.load_verify_locations(ca_file)
        # Allow RSA 1024 keys to be used (for purpose of fast testing)
        ssl_context.set_ciphers('DEFAULT@SECLEVEL=1')
        ssl_context.load_cert_chain(client_cert, keyfile=client_key)

        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_s:
            # Set timeout to 2 seconds to prevent infinite wait time
            client_s.settimeout(2)
            client_s.connect((server_ip, server_socket))
            # Hard coded server hostname
            # TODO: make this either dynamic or not care about hostname
            with ssl_context.wrap_socket(client_s,
                                         server_hostname="serv_cn") as ssl_s:
                print(ssl_s.version())
                echo_loop(ssl_s)

    except ssl.SSLError as e:
        print("SSL error")
        print(e)
    except OSError as e:
        print(e)
    except KeyboardInterrupt as e:
        print("CTRL-C")
        print(e)


if __name__ == "__main__":
    main()
