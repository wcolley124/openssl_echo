#define _POSIX_C_SOURCE 201712L
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <signal.h>

#include <stdatomic.h>
#include <pthread.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

#include <openssl/ssl.h>
#include <openssl/err.h>
#include <openssl/evp.h>

enum { SUCCESS = 0, NULL_GIVEN = 1, GENERIC_SSL_ERROR = 2, THREAD_ADD_FAIL = 4 };

enum { BUFF_SIZE = 1042, THREAD_POOL_SIZE = 8};

enum { THREAD_RUNNING = 1, THREAD_EXITED = 2, THREAD_EMPTY = 0 };

// Used so a thread pool pointer can be put into the ssl_thread struct.
// TODO: There is probably a better way to do this.
typedef struct thread_pool thread_pool;


// Used to signal across threads for a stop signal.
sig_atomic_t stop_sig;

struct shared_message {
	// JQR Item 4.5.4: Locks
	pthread_mutex_t message_mutex;
	size_t message_size;
	char *message;
};

struct ssl_thread {
/*
 * Holds information about a ssl thread.
 */
	thread_pool *current_pool;
	// JQR Item 4.5.4: Threads
	pthread_t thread_id;
	// TODO: possibly make volatile
	// volatile to allow different threads to change the value asynchronously
	// Atomic to ensure the value can be changed by different threads
	// JQR Item 4.5.4: Atomics
	atomic_int thread_running;
	atomic_int new_message;
	int socket_fd;
	SSL *ssl_connection;
	SSL_CTX *ssl_context;
	struct shared_message *common_message;
};

struct thread_pool {
	int array_size;
	struct shared_message *common_message;
	struct ssl_thread *thread_array;
};

struct thread_pool *create_thread_pool(int size)
{
/*
 * Create a thread pool and return a pointer to it.
 * Note: this version does not initialize threads;
 * it only creates a wrapping structure.
 * Return NULL on failure.
 */
	// Do not attempt to create a thread pool with negative or zero size.
	if (size <= 0) {
		return NULL;
	}
	struct thread_pool *new_pool = calloc(1, sizeof(*new_pool));

	if (new_pool == NULL) {
		perror("calloc");
		return NULL;
	}
	new_pool->array_size = size;
	new_pool->thread_array = calloc((size_t)size, sizeof(*(new_pool->thread_array)));
	if (new_pool->thread_array == NULL) {
		perror("calloc");
		free(new_pool);
		new_pool = NULL;
		return NULL;
	}

	// Allocate memory for shared message object
	new_pool->common_message = calloc(1, sizeof(*(new_pool->common_message)));
	if (new_pool->common_message == NULL) {
		perror("calloc");
		free(new_pool->thread_array);
		new_pool->thread_array = NULL;
		free(new_pool);
		new_pool = NULL;
		return NULL;
	}

	// Initialize mutex for shared message object
	if (pthread_mutex_init(&(new_pool->common_message->message_mutex), NULL) != 0) {
		perror("pthread_mutex_init");
		free(new_pool->common_message);
		new_pool->common_message = NULL;
		free(new_pool->thread_array);
		new_pool->thread_array = NULL;
		free(new_pool);
		new_pool = NULL;
		return NULL;
	}

	return new_pool;
}


void destroy_thread_pool(struct thread_pool *current_pool)
{
/*
 * Destroy a thread pool. Cancel all threads within and
 * free all memory.
 */
	if (current_pool == NULL) {
		return;
	}
	// TODO: find better way of checking if threads are done
	int check_val = 0;
	int active_threads = 1;

	while (active_threads != 0) {
		active_threads = 0;
		// Cancel any running threads, then join
		for (int i = 0; i < current_pool->array_size; i++) {
			if ((current_pool->thread_array)[i].thread_running == THREAD_RUNNING) {
				active_threads = 1;
				check_val = pthread_cancel((current_pool->thread_array[i]).thread_id);
				if (check_val != 0) {
					perror("pthread_cancel");
				}
			}
			if ((current_pool->thread_array)[i].thread_running == THREAD_EXITED) {
				check_val = pthread_join((current_pool->thread_array[i]).thread_id, NULL);
				if (check_val != 0) {
					perror("pthread_join");
				}
				(current_pool->thread_array[i]).thread_running = THREAD_EMPTY;
			}
		}
	}
	if (pthread_mutex_destroy(&(current_pool->common_message->message_mutex)) != 0) {
		perror("pthread_mutex_destroy");
	}
	free(current_pool->common_message->message);
	free(current_pool->common_message);
	free(current_pool->thread_array);
	free(current_pool);
}

void sigint_handler(int signal_number)
{
/*
 * Handle CTRL-C SIGINT signal.
 */
	if (signal_number == SIGINT) {
		write(1, "\nSIGINT\n", 8);
		stop_sig = 1;
	}
}

SSL_CTX *initialize_server_context(void)
{
/*
 * Create a SSL_CTX (ssl context) object and return it.
 * This object is used to hold context information about
 * establishing an SSL/TLS session.
 * JQR item 4.6.2: Establish a secure communications channel using an SSL library.
 */
	const SSL_METHOD *new_method = NULL;
	SSL_CTX *new_context = NULL;

	// Initializes internal table of digests and ciphers to lookup
	// ciphers later in the program
	OpenSSL_add_all_algorithms();
	// Load error strings into memory for future error message use.
	SSL_load_error_strings();
	// Instantiates a new generic server method. Specific version methods
	// are deprecated, so version will be set in later function.
	// This object will be automatically freed later, so it does not
	// have to be manually freed.
	new_method = TLS_server_method();

	if (new_method == NULL) {
		ERR_print_errors_fp(stderr);
		return NULL;
	}

	// Instantiate a new context object for SSL/TLS connections
	new_context = SSL_CTX_new(new_method);

	if (new_context == NULL) {
		ERR_print_errors_fp(stderr);
		return NULL;
	}

	// Change this to minimal security so that it doesn't take forever
	// In future, change this back to level 5
	SSL_CTX_set_security_level(new_context, 2);
	// Set the protocol value to the latest version (TLS version 1.3)
	// This will only allow protocols newer than TLS 1.3 to be used.
	int check_ret = SSL_CTX_set_min_proto_version(new_context, TLS1_3_VERSION);

	if (check_ret != 1) {
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(new_context);
		new_context = NULL;
		return NULL;
	}

	// TODO: check return value and make better
	check_ret = SSL_CTX_load_verify_locations(new_context, "certs/ca_cert.pem", NULL);

	if (check_ret != 1) {
		ERR_print_errors_fp(stderr);
		SSL_CTX_free(new_context);
		new_context = NULL;
		return NULL;
	}

	return new_context;
}

int load_ssl_certificate_context(SSL_CTX *ssl_context, char *certificate_file, char *private_key_file)
{
/*
 * Load Certificates and Private key from a given file path.
 * Return SUCCESS on success, and an error value on error.
 */
	if ((ssl_context == NULL) || (certificate_file == NULL) || (private_key_file == NULL)) {
		return NULL_GIVEN;
	}

	// Load a certificate from a given file. This will probably be in the form of an X509 or pem file
	int check_val = SSL_CTX_use_certificate_file(ssl_context, certificate_file, SSL_FILETYPE_PEM);

	if (check_val != 1) {
		ERR_print_errors_fp(stderr);
		return GENERIC_SSL_ERROR;
	}

	// Load a private key from a given file. This will be in the form of a pem file.
	check_val = SSL_CTX_use_PrivateKey_file(ssl_context, private_key_file, SSL_FILETYPE_PEM);

	if (check_val != 1) {
		ERR_print_errors_fp(stderr);
		return GENERIC_SSL_ERROR;
	}

	// Ensure that the private key matches the certificate given.
	check_val = SSL_CTX_check_private_key((const SSL_CTX *)ssl_context);

	if (check_val != 1) {
		fprintf(stderr, "Private Key does not match public certificate\n");
		return GENERIC_SSL_ERROR;
	}

	// Request peer client certificate, if one is available
	SSL_CTX_set_verify(ssl_context, SSL_VERIFY_PEER|SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);

	return SUCCESS;
}

int open_listening_socket(char *port_str)
{
/*
 * Open a listening socket on a given port and return file descriptor.
 * Return negative value on error.
 * Based on Beej's guid
 */
	if (port_str == NULL) {
		fprintf(stderr, "Null string given for port number\n");
		return -1;
	};
	// Ensure port format is correct
	char *end_convert = NULL;
	long port_long = strtol(port_str, &end_convert, 10);

	if ((*end_convert != '\0') || (*port_str == '\0')) {
		fprintf(stderr, "Invalid port number format\n");
		return -1;
	} else if ((port_long <= 0) || (port_long > 65535)) {
		fprintf(stderr, "Port number must be between 1 and 65535\n");
		return -1;
	}

	int socket_file_desc = -1;
	struct addrinfo address_hints = { 0 };
	struct addrinfo *server_info = NULL;
	struct addrinfo *temp_addr = NULL;
	int option_value = 1;

	// Clear out address hints to zero.
	memset(&address_hints, 0, sizeof(address_hints));

	// Only get an address in the IPv4 address space.
	address_hints.ai_family = AF_INET;
	// Specify this will be a TCP connection
	address_hints.ai_socktype = SOCK_STREAM;
	address_hints.ai_flags = AI_PASSIVE;

	int check_return = getaddrinfo(NULL, port_str, &address_hints, &server_info);

	if (check_return != 0) {
		fprintf(stderr, "Getaddrinfo: %s\n", gai_strerror(check_return));
		return -1;
	}
	for (temp_addr = server_info; temp_addr != NULL; temp_addr = temp_addr->ai_next) {
		// Attempt to create a socket from one of the given addresses.
		socket_file_desc = socket(temp_addr->ai_family, temp_addr->ai_socktype, temp_addr->ai_protocol);

		if (socket_file_desc <= 0) {
			perror("server: socket");
			continue;
		}
		// Set options in socket.
		check_return = setsockopt(socket_file_desc, SOL_SOCKET, SO_REUSEADDR, &option_value, sizeof(option_value));
		if (check_return < 0) {
			perror("setsockopt");
			freeaddrinfo(server_info);
			server_info = NULL;
			return -1;
		}

		// Attempt to bind the socket.
		check_return = bind(socket_file_desc, temp_addr->ai_addr, temp_addr->ai_addrlen);
		if (check_return < 0) {
			close(socket_file_desc);
			continue;
		}
		// Only set the listening socket on the first success.
		break;
	}
	freeaddrinfo(server_info);
	server_info = NULL;

	// If no socket was successfully created, return error
	if (temp_addr == NULL) {
		fprintf(stderr, "Server failed to open listening socket\n");
		return -1;
	}
	// TODO: replace 10 with defined value
	check_return = listen(socket_file_desc, 10);

	if (check_return == -1) {
		perror("listen");
		close(socket_file_desc);
		return -1;
	}
	printf("listening on socket %s\n", port_str);
	return socket_file_desc;
}

int show_ssl_certs(SSL *ssl_connection)
{
/*
 * Show the certificates of an ssl connection.
 */
	if (ssl_connection == NULL) {
		return NULL_GIVEN;
	}

	X509 * client_certificate = SSL_get_peer_certificate(ssl_connection);

	if (client_certificate == NULL) {
		printf("No client certificate found\n");
		return SUCCESS;
	}
	printf("Client certificates:\n");
	// Internal struct that MUST NOT BE FREE
	X509_NAME *temp_name = X509_get_subject_name(client_certificate);

	if (temp_name == NULL) {
		X509_free(client_certificate);
		client_certificate = NULL;
		fprintf(stderr, "Unable to get x509 subject name\n");
		return GENERIC_SSL_ERROR;
	}
	// Print the subject name to stdout
	int check_val = X509_NAME_print_ex_fp(stdout, temp_name, 0, 0);

	if (check_val != 1) {
		X509_free(client_certificate);
		client_certificate = NULL;
		fprintf(stderr, "Unable to print x509 subject name\n");
		return GENERIC_SSL_ERROR;
	}

	// Return the issuer name
	temp_name = X509_get_issuer_name(client_certificate);
	if (temp_name == NULL) {
		X509_free(client_certificate);
		client_certificate = NULL;
		fprintf(stderr, "Unable to get x509 issuer name\n");
		return GENERIC_SSL_ERROR;
	}
	// Print the issuer name to stdout.
	// TODO: Debug this
	check_val = X509_NAME_print_ex_fp(stdout, temp_name, 0, 0);
	if (check_val != 1) {
		X509_free(client_certificate);
		client_certificate = NULL;
		fprintf(stderr, "Unable to print x509 issuer name\n");
		return GENERIC_SSL_ERROR;
	}

	puts("\n");
	X509_free(client_certificate);
	client_certificate = NULL;
	return SUCCESS;
}


int establish_ssl_connection(struct ssl_thread *thread_info)
{
/*
 * Establish an SSL connection over a socket, given the current thread
 * state.
 * Return SUCCESS on success.
 */
	SSL_CTX *server_context = thread_info->ssl_context;
	int client_socket = thread_info->socket_fd;

	// Create new SSL structure for client connection.
	thread_info->ssl_connection = SSL_new(server_context);
	SSL *new_ssl_state = thread_info->ssl_connection;

	if (new_ssl_state == NULL) {
		ERR_print_errors_fp(stderr);
		return GENERIC_SSL_ERROR;
	}

	// Set the socket as the encrypted input and output for SSL encryption
	int check_return = SSL_set_fd(new_ssl_state, client_socket);

	if (check_return != 1) {
		ERR_print_errors_fp(stderr);
		return GENERIC_SSL_ERROR;
	}

	if (SSL_accept(new_ssl_state) != 1) {
		ERR_print_errors_fp(stderr);
		return GENERIC_SSL_ERROR;
	}
	// Ignoring return value for this case.
	show_ssl_certs(new_ssl_state);

	check_return = SSL_get_verify_result(new_ssl_state);
	if (check_return != X509_V_OK) {
		fprintf(stderr, "SSL verification of peer certificate failed\n");
		// TODO: print why it failed
		ERR_print_errors_fp(stderr);
		return GENERIC_SSL_ERROR;
	}
	return SUCCESS;
}

void cleanup_ssl_thread(void *thread_void)
{
/*
 * Cleanup a SSL thread.
 * Used for pthread_cleanup
 */
	struct ssl_thread *thread_state = (struct ssl_thread *)thread_void;

	if (thread_state == NULL) {
		thread_state->thread_running = THREAD_EXITED;
		return;
	}
	// If a ssl connection is established, close it.
	if (thread_state->ssl_connection != NULL) {
		SSL_free(thread_state->ssl_connection);
		thread_state->ssl_connection = NULL;
	}
	// If a socket is open, close it
	if (thread_state->socket_fd != 0) {
		close(thread_state->socket_fd);
		thread_state->socket_fd = 0;
	}
	// TODO: find a better way of signaling thread is not active.
	// Remove the thread id from the thread array, marking it as over
	thread_state->thread_running = THREAD_EXITED;
}

int mark_new_message(struct thread_pool *current_pool)
{
/*
 * Marks all threads that there is a new wall message.
 */
	if (current_pool == NULL) {
		return NULL_GIVEN;
	}

	// Iterate through thread array.
	for (int i = 0; i < current_pool->array_size; i++) {
		// If there is a thread running in that location, mark it.
		if ((current_pool->thread_array)[i].thread_running == THREAD_RUNNING) {
			(current_pool->thread_array)[i].new_message = 1;
		}
	}
	return SUCCESS;
}

void *ssl_echo_threaded(void *thread_arg)
{
/*
 * Thread function for an SSL echo connection.
 * Take a previously opened socket and establish an SSL connection
 * through it.
 * Does not return anything.
 * Detached state, so thread does not have to be joined.
 */
	pthread_cleanup_push(&cleanup_ssl_thread, thread_arg);
	struct ssl_thread *thread_state = (struct ssl_thread *)thread_arg;
	int check_return = establish_ssl_connection(thread_state);

	if (check_return != SUCCESS) {
		fprintf(stderr, "Unable to establish SSL connection\n");
		goto SSL_CLEANUP_CONNECTION;
	}

	SSL *ssl_object = thread_state->ssl_connection;

	char recv_buffer[BUFF_SIZE] = { 0 };

	const char *send_message = "Hello from the server!\n";
	int bytes_written = SSL_write(ssl_object, send_message, strlen(send_message));

	if (bytes_written < 0) {
		ERR_print_errors_fp(stderr);
		goto SSL_CLEANUP_CONNECTION;
	} else if (bytes_written != (int)strlen(send_message)) {
		fprintf(stderr, "Error writing to ssl connection\n"
				"Tried to write %ld, instead wrote %d\n",
				strlen(send_message),
				bytes_written);
		goto SSL_CLEANUP_CONNECTION;
	}
	// Run until a stop signal is received.
	while (stop_sig != 1) {
		// Check if there is a new message to be sent.
		// TODO: possibly break this into separate function.
		if (thread_state->new_message == 1) {
			int successful_send = 0;
			int check_lock = pthread_mutex_lock(&(thread_state->common_message->message_mutex));

			// If the mutex fails to be acquired, exit.
			if (check_lock != 0) {
				perror("pthread_mutex_lock");
				break;
			}
			// Send the message.
			bytes_written = SSL_write(ssl_object, thread_state->common_message->message,
					thread_state->common_message->message_size);

			if (bytes_written < 0) {
				ERR_print_errors_fp(stderr);
				successful_send = 1;
			} else if ((size_t)bytes_written != thread_state->common_message->message_size) {
				fprintf(stderr, "ERROR writing new common message to socket\n"
								"Sent bytes do not match message size\n");
				successful_send = 1;
			}
			// Release the message mutex.
			check_lock = pthread_mutex_unlock(&(thread_state->common_message->message_mutex));
			if (check_lock != 0) {
				perror("pthread_mutex_unlock");
				successful_send = 1;
			}
			// If there was a failure to send the message, break out of echo loop.
			if (successful_send != 0) {
				break;
			}
			// Mark that there is not a new message for this thread.
			thread_state->new_message = 0;
		}

		// Read from the socket into the receive buffer
		int bytes_read = SSL_read(ssl_object, &recv_buffer, BUFF_SIZE - 1);

		if (bytes_read > 0) {
			// Ensure null terminating character at the end of the buffer.
			recv_buffer[bytes_read] = '\0';
			printf("Message received: \"%s\"\n", recv_buffer);
		} else {
			ERR_print_errors_fp(stderr);
			break;
		}
		// Check for quit message.
		if ((strncmp(recv_buffer, "quit", BUFF_SIZE) == 0) || (strncmp(recv_buffer, "quit\n", BUFF_SIZE) == 0)) {
			printf("Received quit signal\n");
			goto SSL_CLEANUP_CONNECTION;
		}
		// Check for wall message.
		if (strncmp(recv_buffer, "WALL:", 5) == 0) {
			printf("Received WALL: signal\n");
			// If wall message received, write new wall message.
			int check_lock = pthread_mutex_lock(&(thread_state->common_message->message_mutex));

			// If the mutex fails to be acquired, exit.
			if (check_lock != 0) {
				perror("pthread_mutex_lock");
				break;
			}
			char *new_message = strndup((const char *)&(recv_buffer[5]), BUFF_SIZE - 6);

			if (new_message == NULL) {
				perror("strndup");
			} else {
				free(thread_state->common_message->message);
				thread_state->common_message->message = new_message;
				thread_state->common_message->message_size = strnlen(new_message, BUFF_SIZE - 6);
				// Mark all threads as having a new message.
				mark_new_message(thread_state->current_pool);
			}

			// Release message mutex.
			check_lock = pthread_mutex_unlock(&(thread_state->common_message->message_mutex));
			if (check_lock != 0) {
				perror("pthread_mutex_unlock");
				break;
			}

		}
		// Write the entire buffer out before reading more in.
		bytes_written = 0;
		while (bytes_written < bytes_read) {

			int temp_bytes_written = SSL_write(ssl_object, &(recv_buffer[bytes_written]),
					bytes_read - bytes_written);

			if (temp_bytes_written < 0) {
				ERR_print_errors_fp(stderr);
				goto SSL_CLEANUP_CONNECTION;
			}
			bytes_written += temp_bytes_written;
		}
	}

SSL_CLEANUP_CONNECTION:
	// Run cleanup
	// TODO: have this auto execute at pthread_exit
	pthread_cleanup_pop(1);

	pthread_exit(NULL);
}


int add_thread_pool(struct thread_pool *current_pool, int client_socket, SSL_CTX *server_context)
{
/*
 * Add a new task to the thread pool.
 * Return SUCCESS on successful addition of socket to thread pool.
 * Return THREAD_ADD_FAIL on failure.
 * Note, this is currently not very generic.
 * TODO: make more generic
 */
	if ((current_pool == NULL) || (client_socket < 0)) {
		return THREAD_ADD_FAIL;
	}

	int check_thread = 0;
	struct ssl_thread *thread_location = NULL;

	// TODO: do this with a mutex or semaphore?
	for (int i = 0; i < current_pool->array_size; i++) {
		if ((current_pool->thread_array)[i].thread_running == THREAD_EXITED) {
			check_thread = pthread_join((current_pool->thread_array[i]).thread_id, NULL);
			if (check_thread != 0) {
				perror("pthread_join");
			}
			(current_pool->thread_array[i]).thread_running = THREAD_EMPTY;
		}
		if ((current_pool->thread_array)[i].thread_running == THREAD_EMPTY) {
			thread_location = &((current_pool->thread_array)[i]);
			break;
		}
	}
	if (thread_location == NULL) {
		return THREAD_ADD_FAIL;
	}

	thread_location->socket_fd = client_socket;
	thread_location->ssl_context = server_context;
	thread_location->thread_running = 1;
	thread_location->current_pool = current_pool;
	thread_location->common_message = current_pool->common_message;
	check_thread = pthread_create(&(thread_location->thread_id),
			NULL, &ssl_echo_threaded, thread_location);

	if (check_thread != 0) {
		perror("pthread_create");
		thread_location->socket_fd = 0;
		thread_location->ssl_context = NULL;
		thread_location->thread_running = 0;
		return 1;
	}


	return SUCCESS;
}

int main(void)
{
/*
 * Example SSL server, using examples from
 * https://aticleworld.com/ssl-server-client-using-openssl-in-c/
 */

	struct thread_pool *current_pool = create_thread_pool(THREAD_POOL_SIZE);

	if (current_pool == NULL) {
		fprintf(stderr, "Error creating thread pool\n");
		return 1;
	}

	// Return value is safe to discard according to man page
	SSL_library_init();

	SSL_CTX *server_context = initialize_server_context();

	if (server_context == NULL) {
		fprintf(stderr, "Error initializing server context\n");
		return 1;
	}

	// TODO: Use command line arguments and optopt to specify which certificate to use
	int check_return = load_ssl_certificate_context(server_context, (char *)"certs/server_cert.pem", (char *)"certs/server_key.pem");

	if (check_return != SUCCESS) {
		fprintf(stderr, "Error loading certificates to SSL context\n");
		SSL_CTX_free(server_context);
		server_context = NULL;
		return 1;
	}

	// TODO: Use command line arguments to specify port number
	int listening_socket = open_listening_socket((char *)"5555");

	if (listening_socket < 0) {
		fprintf(stderr, "Failed to open a listening socket\n");
		SSL_CTX_free(server_context);
		server_context = NULL;
		return 1;
	}

	struct sigaction new_sig_action = { 0 };

	memset(&new_sig_action, 0, sizeof(new_sig_action));
	new_sig_action.sa_handler = &sigint_handler;
	if (sigaction(SIGINT, &new_sig_action, NULL) == -1) {
		perror("sigaction\n");
		close(listening_socket);
		SSL_CTX_free(server_context);
		server_context = NULL;
		return 1;
	}

	while (stop_sig != 1) {
		struct sockaddr_in connection_address = { 0 };
		socklen_t addr_length = sizeof(connection_address);

		int client_socket = accept(listening_socket, (struct sockaddr *)&connection_address, &addr_length);

		if (client_socket < 0) {
			perror("accept");
			break;
		}
		printf("Connection :%s:%d\n", inet_ntoa(connection_address.sin_addr), ntohs(connection_address.sin_port));

		check_return = add_thread_pool(current_pool, client_socket, server_context);
		if (check_return != SUCCESS) {
			close(client_socket);
			fprintf(stderr, "Error adding connection to thread pool\n");
		}
	}
	destroy_thread_pool(current_pool);

	close(listening_socket);
	SSL_CTX_free(server_context);
	server_context = NULL;
	return 0;
}
